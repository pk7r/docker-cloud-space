#!/bin/bash

identify_os() {
    case "$(uname -s)" in
        Linux*)
            echo "1" # Linux
            ;;
        CYGWIN* | MINGW32* | MSYS* | MINGW*)
            echo "2" # Windows
            ;;
        Darwin*)
            echo "3" # macOS
            ;;
        *)
            echo "0" # Unknown or unsupported OS
            ;;
    esac
}