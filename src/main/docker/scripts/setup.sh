#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "$SCRIPT_DIR/docker.sh"
source "$SCRIPT_DIR/swarm.sh"
source "$SCRIPT_DIR/envs.sh"
source "$SCRIPT_DIR/os.sh"

setup() {
  echo "Initializing docker cloud setup.";
  OS_TYPE=$(identify_os)
  case $OS_TYPE in
  1)
    echo "Running os Linux"
    ;;
  2)
    echo "Unknown or unsupported OS"
    exit 0
    ;;
  3)
    echo "Running os macOS"
    ;;
  0)
    echo "Unknown or unsupported OS"
    exit 0
    ;;
  esac
  add_env "OS_CODE" "$OS_TYPE" "false"
  check_docker_installed
  ask_for_swarm_enable
  create_network tunnel
  setup_tunnel
  setup_portainer
  setup_kuma
  setup_space
}

setup_tunnel() {
  # shellcheck disable=SC2162
  read -p "Do you want to install Cloudflare Tunnel? (y/n): " choice
  case "$choice" in
      [Yy]*)
          # shellcheck disable=SC2162
          read -p "Cloudflare Tunnel Token: " token
          add_env "CLOUDFLARE_TUNNEL_TOKEN" "$token"
          deploy cloudflare
          ;;
      [Nn]*)
          echo "Cloudflare Tunnel will not be installed."
          ;;
      *)
          echo "Invalid choice. Please enter 'y' or 'n'."
          setup_tunnel
          ;;
  esac
}

setup_portainer() {
  # shellcheck disable=SC2162
  read -p "Do you want to install Portainer? (y/n): " choice
  case "$choice" in
      [Yy]*)
          deploy portainer
          ;;
      [Nn]*)
          echo "Portainer will not be installed."
          ;;
      *)
          echo "Invalid choice. Please enter 'y' or 'n'."
          setup_portainer
          ;;
  esac
}

setup_kuma() {
  # shellcheck disable=SC2162
  read -p "Do you want to install Uptime Kuma? (y/n): " choice
  case "$choice" in
      [Yy]*)
          deploy kuma
          ;;
      [Nn]*)
          echo "Uptime Kuma will not be installed."
          ;;
      *)
          echo "Invalid choice. Please enter 'y' or 'n'."
          setup_kuma
          ;;
  esac
}

setup_space() {
  # shellcheck disable=SC2162
  read -p "Do you want to install Jetbrains Space? (y/n): " choice
  case "$choice" in
      [Yy]*)
          deploy space
          ;;
      [Nn]*)
          echo "Jetbrains Space will not be installed."
          ;;
      *)
          echo "Invalid choice. Please enter 'y' or 'n'."
          setup_space
          ;;
  esac
}