#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "$SCRIPT_DIR/os.sh"
source "$SCRIPT_DIR/envs.sh"

ask_for_docker_installation() {
    # shellcheck disable=SC2162
    read -p "Docker is not installed. Do you want to install Docker? (y/n): " choice
    case "$choice" in
        [Yy]*)
            install_docker
            ;;
        [Nn]*)
            echo "Exiting script. Docker will not be installed."
            exit 0
            ;;
        *)
            echo "Invalid choice. Please enter 'y' or 'n'."
            ask_for_docker_installation
            ;;
    esac
}

check_docker_installed() {
    # Check if Docker is installed
    if ! command -v docker &> /dev/null; then
        ask_for_docker_installation
    else
        echo "Docker is already installed on this machine."
    fi
}

install_docker() {
    echo "Installing Docker..."

    case $(identify_os) in
        1)
            install_docker_linux
            ;;
        3)
            install_docker_macos
            ;;
        2)
            install_docker_windows
            ;;
        *)
            echo "Unsupported operating system: $(uname -s)"
            exit 1
            ;;
    esac
}

install_docker_linux() {
    echo "Installing Docker on Linux..."

    # Update the package index and install required packages
    sudo apt-get update
    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

    # Add Docker repository key and repository
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    # Install Docker Engine
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io

    # Add the current user to the docker group to run Docker without sudo
    sudo usermod -aG docker $USER

    echo "Docker has been installed successfully on Linux."
}

install_docker_macos() {
    echo "Please install Docker Desktop for macOS from the following link:"
    echo "https://www.docker.com/products/docker-desktop"
    echo "After installing Docker Desktop correctly on Mac, please re-run docker cloud startup script."
    exit 0
}

install_docker_windows() {
    echo "Please install Docker Desktop for Windows from the following link:"
    echo "https://www.docker.com/products/docker-desktop"
    echo "After installing Docker Desktop correctly on PC, please re-run docker cloud startup script."
    exit 0
}

deploy() {
    local stack_name=$1
    local compose_file="$SCRIPT_DIR/../stacks/$stack_name.yml"

    if [ ! -f "$compose_file" ]; then
        echo "Error: The specified Compose file '$stack_name.yml' does not exist in the '/stacks' directory."
        exit 1
    fi

    echo "Deploying stack: $stack_name"

    docker stack deploy -c "$compose_file" "$stack_name"
}

create_network() {
  local network_name=$1
  echo "Creating network: $network_name"
  docker network create --driver overlay $network_name
}