#!/bin/bash

add_env() {
    local key=$1
    local value=$2
    export "$key=$value"
}

get_env() {
    local key=$1
    eval "echo \$$key"
}