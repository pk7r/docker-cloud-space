#!/bin/bash

is_docker_running() {
    if docker info &> /dev/null; then
        return 0 # Docker is running
    else
        return 1 # Docker is not running
    fi
}

is_swarm_enabled() {
    if ! is_docker_running; then
        echo "Docker is not running. Please start Docker and try again."
        exit 1
    fi

    if docker info | grep -q "Swarm: active"; then
        return 0 # Swarm is enabled
    else
        return 1 # Swarm is not enabled
    fi
}

ask_for_swarm_enable() {
    if is_swarm_enabled; then
        echo "Docker Swarm is already enabled on this machine."
        return
    fi
    enable_docker_swarm
}

enable_docker_swarm() {
    # shellcheck disable=SC2162
    read -p "Do you want to create a new swarm or join an existing swarm? (N/E): " choice
    case "$choice" in
        [Ee]*)
            join_swarm
            ;;
        [Nn]*)
            init_swarm
            ;;
        *)
            echo "Invalid choice. Please enter 'new' or 'existing'."
            enable_docker_swarm
            ;;
    esac
}

init_swarm() {
    docker swarm init
}

join_swarm() {
    # shellcheck disable=SC2162
    read -p "Enter the token to join the Docker Swarm: " token
    # shellcheck disable=SC2162
    read -p "Enter the IP address of the manager node: " ip
    docker swarm join --token $token $ip:2377
}